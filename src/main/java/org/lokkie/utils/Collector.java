package org.lokkie.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Part of org.lokkie.utils. Created in jvm-base
 *
 * @author lokkie
 * @version 0.1
 */
public class Collector {
    @SuppressWarnings("unchecked")
    public static <T> T[] collect(ArrayList<T> income, Class<T> tClass) {
        return income.toArray((T[]) Array.newInstance(tClass, 0));
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] collect(List<T> income, Class<T> tClass) {
        return income.toArray((T[]) Array.newInstance(tClass, 0));
    }
}
