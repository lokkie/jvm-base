package org.lokkie.annotation

/**
 * An annotation that indicates this member should not be included in MixIn operation.
 * Used together with Mixable class
 *
 * @author lokkie
 * @version 0.1
 * @see org.lokkie.types.Mixable
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class SkipMixing(
        /**
         * @return should skip in all cases (false) or only null-valued fields (true)
         */
        val nullOnly: Boolean = false)