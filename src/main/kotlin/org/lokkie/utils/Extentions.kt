package org.lokkie.utils

import java.sql.ResultSet

/**
 * Part of org.lokkie.utils. Created in dp-utils
 *
 * @author lokkie
 * @version 0.1
 */
operator fun Regex.contains(text: CharSequence): Boolean = this.matches(text)

fun ResultSet.moveToFirst(): ResultSet = this.first().let { this }