package org.lokkie.utils

/**
 * Part of org.lokkie.types. Created in web
 *
 * @author lokkie
 * @version 0.1
 */
object GlobalExceptionHandler {

    @JvmStatic
    private val handlers = ArrayList<ExceptionHandler>()

    @JvmStatic
    fun registerHandler(handler: ExceptionHandler) {
        if (handlers.isEmpty()) {
            Thread.setDefaultUncaughtExceptionHandler { t, e -> handleAll(t, e) }
        }
        handlers.add(handler)
    }

    @JvmStatic
    fun registerHandler(handler: (t: Thread, e: Throwable) -> Boolean) {
        registerHandler(object : ExceptionHandler {
            override fun handle(t: Thread, e: Throwable) = handler(t, e)
        })
    }

    @JvmStatic
    private fun handleAll(t: Thread, e: Throwable) {
        if (!handlers.map { h -> h.handle(t, e) }.any { it }) {
            e.printStackTrace()
            throw e
        }
    }

    @FunctionalInterface
    interface ExceptionHandler {
        fun handle(t: Thread, e: Throwable): Boolean
    }
}
