package org.lokkie.utils

import java.lang.reflect.Field

/**
 * Part of org.lokkie.utils. Created in kotlin-argparser
 *
 * @author lokkie
 * @version 0.1
 */

object Reflection {
    @JvmStatic
    fun getAllFields(tClass: Class<*>): Array<Field> = tClass.allFields
}

val Class<*>.allFields: Array<Field>
        get() = (if (this.superclass == null) arrayOf() else this.superclass.allFields) + declaredFields