package org.lokkie.utils

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

/**
 * Part of org.lokkie.utils. Created in web
 *
 * @author lokkie
 * @version 0.1
 */
object Hash {
    @JvmStatic
    fun sha1(string: String): String =
            hexBytes(getAlgorithm("SHA-1").digest(string.toByteArray(Charsets.UTF_8)))

    private fun getAlgorithm(name: String): MessageDigest = try {
        MessageDigest.getInstance(name)
    } catch (e: NoSuchAlgorithmException) {
        throw IllegalArgumentException(e)
    }

    private fun hexBytes(byteArray: ByteArray): String {
        val toDigits = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val l = byteArray.size
        val out = CharArray(l shl 1)
        var i = 0
        var j = 0
        while (i < l) {
            out[j++] = toDigits[(240 and byteArray[i].toInt()).ushr(4)]
            out[j++] = toDigits[15 and byteArray[i].toInt()]
            i++
        }
        return String(out)
    }
}