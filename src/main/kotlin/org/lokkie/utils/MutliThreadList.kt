package org.lokkie.utils

import java.util.Collections
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Part of org.lokkie.utils. Created in dp-utils
 *
 * @author lokkie
 * @version 0.1
 */
/**
 * Taken from StackOverflow
 * @link https://stackoverflow.com/questions/34697828/parallel-operations-on-kotlin-collections
 */
fun <T, R> Iterable<T>.mtMap(
        numThreads: Int = Runtime.getRuntime().availableProcessors() - 2,
        exec: ExecutorService = Executors.newFixedThreadPool(numThreads),
        transform: (T) -> R): List<R> {

    // default size is just an inlined version of kotlin.collections.collectionSizeOrDefault
    val defaultSize = if (this is Collection<*>) this.size else 10
    val destination = Collections.synchronizedList(ArrayList<R>(defaultSize))

    for (item in this) {
        exec.submit { destination.add(transform(item)) }
    }

    exec.shutdown()
    exec.awaitTermination(1, TimeUnit.DAYS)

    return ArrayList<R>(destination)
}

fun <T, C : Iterable<T>> C.mtOnEach(
        numThreads: Int = Runtime.getRuntime().availableProcessors() - 2,
        exec: ExecutorService = Executors.newFixedThreadPool(numThreads),
        action: (T) -> Unit): C {
    val result = apply { for (element in this) exec.submit { action(element) } }

    exec.shutdown()
    exec.awaitTermination(3, TimeUnit.MINUTES)

    return result
}