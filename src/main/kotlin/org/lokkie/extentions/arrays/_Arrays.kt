@file:kotlin.jvm.JvmMultifileClass
@file:kotlin.jvm.JvmName("ArraysKt")

package org.lokkie.extentions.arrays

import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Performs the given [action] on each element in [threadCount] threads.
 *
 * @param threadCount number of threads to allow in the pool.
 *      By default 2xCoresCount
 * @param action closure to execute in each thread
 */
public inline fun <T> Array<out T>.forEachPool(threadCount: Int = Runtime.getRuntime().availableProcessors() * 2,
    crossinline action: (T) -> Unit) = forEachPool(threadCount, {}, action)

/**
 * Performs the given [action] on each element in 2xCoresCount threads, and perform [after] on each element
 * in main thread, taking result of [action].
 *
 * @param after Thread safe closure after each action. Takes result of action
 * @param action closure to execute in each thread
 */
public inline fun <T, V> Array<out T>.forEachPool(crossinline after: (V) -> Unit, crossinline action: (T) -> V) =
        forEachPool(Runtime.getRuntime().availableProcessors()*2, after, action)

/**
 * Performs the given [action] on each element in [threadCount] threads, and perform [after] on each element
 * in main thread, taking result of [action].
 *
 * @param threadCount number of threads to allow in the pool.
 *      By default 2xCoresCount
 * @param after Thread safe closure after each action. Takes result of action
 * @param action closure to execute in each thread
 */
public inline fun <T, V> Array<out T>.forEachPool(threadCount: Int,
    crossinline after: (V) -> Unit,
    crossinline action: (T) -> V) {
    // Creating ThreadPool with BlockingQueue
    val pool = ThreadPoolExecutor(threadCount, threadCount, 300L, TimeUnit.MILLISECONDS, LinkedBlockingDeque())

    // Add action for each element int poolQueue
    for (element in this) pool.execute {
        val result = action(element)
        synchronized(this) {
            after(result)
        }
    }

    // Wait for pool ends working
    while (pool.activeCount > 0) Thread.sleep(100)

    // Shutting down pool
    pool.shutdown()
}