package org.lokkie.types

import java.util.Date
import java.text.SimpleDateFormat
import java.time.Instant

/**
 * Allows to work with variant date formats (e.g. atlassian date)
 *
 * @author lokkie
 * @version 0.1
 */
class UnixTimestamp {
    private var value: Long = 0

    /**
     * Getter in Date format
     *
     * @return `java.util.Date` with parsed value
     */
    val date: Date
        get() = Date.from(if (9999999999L < value) Instant.ofEpochMilli(this.value) else Instant.ofEpochSecond(this.value))

    /**
     * Getter for UnixTimestamp
     *
     * @return UnixTimestamp in `long`
     */
    val long: Long
        get() = value

    /**
     * Constructs object from `Date`
     *
     * @param date Timr in `Date` representation
     */
    constructor(date: Date) : this(date.time)

    /**
     * Constructs object from `long` representation
     *
     * @param value UnixTimestamp in `long` representation
     */
    constructor(value: Long) {
        this.value = value
    }

    /**
     * Constructs object from `String` representation of unixTimestamp
     *
     * @param s UnixTime in `String` representation
     * @throws NumberFormatException on incorrect long
     */
    @Throws(NumberFormatException::class)
    constructor(s: String) {
        value = s.toLong()
    }

    fun asString(): String {
        return SimpleDateFormat("dd.MM.YYYY HH:mm:ss").format(date)
    }
}
